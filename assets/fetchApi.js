//suhtlus serveriga
async function fetchCompanies() {
    try {
        let response = await fetch(API_URL + '/companies');
        let companies = await response.json();
        return companies;
    } catch (e) {
        console.log("Mingi jama!", e);
    }
}

async function fetchCompany(id) {
    try {
        let response = await fetch(API_URL + '/companies/' + id);
        let company = await response.json();
        return company;
    } catch (e) {
        console.log("Ettevõtet ei leitud!", e);
    }
}

async function deleteCompany(id) {
    try {
        let result = await fetch(API_URL + '/companies/' + id, {
            method: 'DELETE'
        });
    } catch (e) {
        console.log('Kustutamine ebaõnnestus!', e);
    }
}

async function saveCompany(company) {
    let requestUrl = API_URL + '/companies';
    let conf = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json', },
        body: JSON.stringify(company)
    };
    let result = await fetch(requestUrl, conf);
}